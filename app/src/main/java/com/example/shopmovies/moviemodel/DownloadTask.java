package com.example.shopmovies.moviemodel;

import android.app.Activity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.shopmovies.ui.homemovie.MoviesFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class DownloadTask extends AsyncTask<Void, MovieModel, Void> {

    // List of movies to use in adapter
    private List<MovieModel> moviesList = new ArrayList<MovieModel>();

    private Activity mContext;

    private OpenStream openStream;

    public DownloadTask(Activity contextParent) {
        this.mContext = contextParent;
    }

    @Override
    protected void onPreExecute(){
        // This runs on the UI thread before the background thread executes.
        super.onPreExecute();
        // Do pre-thread tasks such as initializing variables.

    }

    @Override
    protected Void doInBackground(Void... params) {
        // Get Json
        openStream = new OpenStream();
        String result = null;
        result = openStream.getJson();

        // Parse Json
        try {
            // Get JsonArray without key
            // Create a new instance of a JSONArray
            JSONArray array = new JSONArray(result);


            for (int i = 0; i < array.length(); i++) {
                // Get new Json Object from Json Array
                JSONObject jsonObject = array.getJSONObject(i);

                // Parse an object
                String title = jsonObject.getString("title");
                String imageLink = jsonObject.getString("image");
                String price = jsonObject.getString("price");

                // Download bitmap Image
                Bitmap bmp = bitmapImage(imageLink);

                // Create Movie Model to set
                MovieModel movieModel = new MovieModel();
                movieModel.setTitle(title);
                movieModel.setImage(bmp);
                movieModel.setPrice(price);


                // Update model to onProgressUpdate of AsyncTask
                publishProgress(movieModel);


            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


        return null;

    }



    @Override
    protected void onProgressUpdate(MovieModel ...params) {
        // Runs on the UI thread after publishProgress is invoked
        super.onProgressUpdate(params);

        // Update new Movie object has been downloaded to RecyclerView
        moviesList.add(params[0]);

        // If list empty, app will crash
        if (moviesList.size() != 0) {
            MoviesFragment moviesFragment = new MoviesFragment();
            moviesFragment.setupRecyclerView(mContext, moviesList);
        }


    }


    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        // This runs on the UI thread after complete execution of the doInBackground() method
        // This function receives result(String s) returned from the doInBackground() method.
        // Update UI with the found string.

        if(isCancelled()) {
            Toast.makeText(mContext, "Cancel Loading", Toast.LENGTH_SHORT).show();
            return;
        } else {
            Toast.makeText(mContext, "Loading Complete", Toast.LENGTH_SHORT).show();
        }

    }


    // Open URL and download image
    public static Bitmap bitmapImage(String url) {
        Bitmap bmp = null;
        try {
            InputStream in = new URL(url).openStream();
            bmp = BitmapFactory.decodeStream(in);
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }

        return bmp;
    }


    // Bitmap Download and Set Image for User Account
    public static class BitmapDownloaderTask extends AsyncTask<String, Void, Bitmap> {
        private String url;
        private final WeakReference<ImageView> imageViewReference;

        public BitmapDownloaderTask(ImageView imageView, String url) {
            imageViewReference = new WeakReference<ImageView>(imageView);
            this.url = url;
        }


        @Override
        // Actual download method, run in the task thread
        protected Bitmap doInBackground(String... params) {
            // params comes from the execute() call: params[0] is the url.
            return bitmapImage(url);
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);


        }

        @Override
        // Once the image is downloaded, associates it to the imageView
        protected void onPostExecute(Bitmap bitmap) {
            if (isCancelled()) {
                bitmap = null;
            }

            if (imageViewReference != null) {
                ImageView imageView = imageViewReference.get();
                if (imageView != null) {
                    imageView.setImageBitmap(bitmap);
                }
            }

        }
    }
}
