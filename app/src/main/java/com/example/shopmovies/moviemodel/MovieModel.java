package com.example.shopmovies.moviemodel;

import android.graphics.Bitmap;

public class MovieModel {
    private String title;
    private Bitmap image;
    private String price;

    public MovieModel() {

    }

    public MovieModel(String title, Bitmap image, String price) {
        this.title = title;
        this.image = image;
        this.price = price;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPrice() {
        return price;
    }
}
