package com.example.shopmovies.moviemodel;



import java.io.BufferedReader;

import java.io.InputStreamReader;

import java.net.HttpURLConnection;
import java.net.URL;


public class OpenStream {

    public String getJson() {
        String result = null;
        try {
            URL url = new URL("https://api.androidhive.info/json/movies_2017.json");
            // Set up connection
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.connect();

            if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                InputStreamReader inputStreamReader = new InputStreamReader(conn.getInputStream());

                BufferedReader reader = new BufferedReader(inputStreamReader);

                StringBuilder stringBuilder = new StringBuilder();

                // Temporary variable to get new info
                String temp;

                while ((temp = reader.readLine()) != null) {
                    stringBuilder.append(temp);
                }
                result = stringBuilder.toString();
            } else {
                result = "error";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }



}
