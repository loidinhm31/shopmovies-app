package com.example.shopmovies;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import android.os.Bundle;



import com.example.shopmovies.login.SharedPrefsManager;

import com.example.shopmovies.login.facebooklogin.user.GetUserCallback;
import com.example.shopmovies.login.facebooklogin.user.User;
import com.example.shopmovies.login.facebooklogin.user.UserRequest;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;




public class ProcessUserInfoActivity extends Activity
        implements GetUserCallback.IGetUserResponse {

    public Context context;
    private SharedPrefsManager prefs;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        prefs = SharedPrefsManager.getInstance();

        // Check state log in if false, back to Login
        if (prefs.getLogged_IN(ProcessUserInfoActivity.this)) {

            // Check type log in to record user information
            if (prefs.getString(this,"LOGIN_TYPE", "not google").equals("google")) {
                // Recording user info of Google to SharedPrefs
                onCompletedGoogle();
            } else {
                // Call UserCallback to record user info (from Facebook SDK)
                UserRequest.makeUserRequest(new GetUserCallback(ProcessUserInfoActivity.this).getCallback());
            }

            Intent NextScreen = new Intent(this,
                    MainActivity.class);
            startActivity(NextScreen);


        } else {
            Intent intent = new Intent(ProcessUserInfoActivity.this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);

        }
        finish();

    }



    public void onCompletedGoogle() {
        // Check for existing Google Sign In account, if the user is already signed in
        // the GoogleSignInAccount will be non-null.
        GoogleSignInAccount googleAccount = GoogleSignIn.getLastSignedInAccount(this);

        // Google's user have not avatar, which will not have an image link, check this to avoid crash
        String photo = null;
        if (googleAccount.getPhotoUrl() != null) {
            photo = googleAccount.getPhotoUrl().toString();
        }

        // Save info to SharedPrefs
        prefs = SharedPrefsManager.getInstance();
        prefs.setUserInfo(this,
                googleAccount.getId(),
                photo,
                googleAccount.getDisplayName(),
                googleAccount.getEmail(),
                null);
    }



    // This interface to get user info of Facebook (from Facebook SDK)
    @Override
    public void onCompleted(User user) {
        // Save info to SharedPrefs
        prefs = SharedPrefsManager.getInstance();
        prefs.setUserInfo(this,
                user.getId(),
                user.getPicture().toString(),
                user.getName(),
                user.getEmail(),
                user.getBirthday());
    }
}
