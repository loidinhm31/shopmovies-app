package com.example.shopmovies.ui.user;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.shopmovies.R;
import com.example.shopmovies.login.SharedPrefsManager;

import com.example.shopmovies.moviemodel.DownloadTask;

public class UserFragment extends Fragment {

    public Context context;

    private DownloadTask.BitmapDownloaderTask downloaderTask;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_user, container, false);

        // Get user info saved from SharedPrefs in ProcessUserInfoActivity Class
        SharedPrefsManager prefs = SharedPrefsManager.getInstance();

        String picture = prefs.getString(getContext(), "USER_PICTURE", null);

        ImageView userAvatar = root.findViewById(R.id.user_photo);
        // Checking request condition to avoid crash
        if (picture != null) {
            // Call async task to download and auto set up image
            downloaderTask = new DownloadTask.BitmapDownloaderTask(userAvatar, picture);
            downloaderTask.execute();
        }


        String name = prefs.getString(getContext(),"USER_NAME", null);

        String email = prefs.getString(getContext(),"USER_EMAIL", "NOT AVAILABLE");

        String birthday = prefs.getString(getContext(), "USER_BIRTHDAY", "NOT AVAILABLE");


        // Set UI
        TextView userName = root.findViewById(R.id.user_name);
        userName.setText(name);

        TextView userEmail = root.findViewById(R.id.user_email);
        userEmail.setText(email);

        TextView userBirth = root.findViewById(R.id.user_birthday);
        userBirth.setText(birthday);

        return root;
    }

}
