package com.example.shopmovies.ui.homemovie;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.shopmovies.R;
import com.example.shopmovies.moviemodel.DownloadTask;
import com.example.shopmovies.moviemodel.MovieModel;

import java.util.List;

public class MoviesFragment extends Fragment {

    private MovieRecyclerAdapter mAdapter;
    private DownloadTask asyncTask;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_shopmovies, container, false);

        // Call Async Task to download information
        asyncTask = new DownloadTask(getActivity());
        asyncTask.execute();

        return root;
    }


    @Override
    public void onPause() {
        super.onPause();
        // Cancel Download Task to avoid crash
        asyncTask.cancel(true);
    }



    public void setupRecyclerView(Activity context, List<MovieModel> dataSet) {
        RecyclerView moviesRecycler = context.findViewById(R.id.movies_recycler_view);

        // Use linear layout manager
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 3);
        moviesRecycler.setLayoutManager(layoutManager);

        // Specify an adapter
        mAdapter = new MovieRecyclerAdapter(dataSet);
        moviesRecycler.setAdapter(mAdapter);

        mAdapter.notifyDataSetChanged();

    }


}
