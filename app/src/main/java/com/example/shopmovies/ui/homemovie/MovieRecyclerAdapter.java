package com.example.shopmovies.ui.homemovie;

import android.app.Activity;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;


import androidx.recyclerview.widget.RecyclerView;


import com.example.shopmovies.R;
import com.example.shopmovies.moviemodel.MovieModel;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;

import java.util.List;

public class MovieRecyclerAdapter
        extends RecyclerView.Adapter<MovieRecyclerAdapter.ViewHolder> {

    private List<MovieModel> mMoviesList;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public View view;
        public TextView titleView, priceView;
        public ImageView imageView;
        public Button shareFbBtn;

        public ViewHolder(View v) {
            super(v);
            view = v;
            titleView = v.findViewById(R.id.movie_title);
            imageView = v.findViewById(R.id.movie_img);
            priceView = v.findViewById(R.id.movie_price);
            shareFbBtn = v.findViewById(R.id.shareFb_button);
        }
    }

    // Provide facebook_button suitable constructor (depends on the kind of dataset)
    public MovieRecyclerAdapter(List<MovieModel> data) {
        mMoviesList = data;
        notifyDataSetChanged();
    }

    // Create new views (invoked by the layout manager)
    @Override
    public MovieRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                     int viewType) {
        // create facebook_button new view
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.movie_item_view, parent, false);

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of facebook_button view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that

        // Set title of movie from model
        holder.titleView.setText(mMoviesList.get(position).getTitle());

        // Set image of movie from model
        holder.imageView.setImageBitmap(mMoviesList.get(position).getImage());

        // Set price of movie from model
        holder.priceView.setText((mMoviesList.get(position).getPrice()));


        // Share the image of the movie on Facebook
        holder.shareFbBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShareDialog shareDialog = new ShareDialog((Activity) view.getContext());
                if (shareDialog.canShow(ShareLinkContent.class)) {

                    // Get bitmap from list
                    Bitmap bmp = mMoviesList.get(holder.getAdapterPosition()).getImage();

                    // Create Builder
                    SharePhoto photo = new SharePhoto.Builder()
                            .setBitmap(bmp)
                            .build();

                    SharePhotoContent content = new SharePhotoContent.Builder()
                            .addPhoto(photo)
                            .build();

                    shareDialog.show(content);
                }

            }
        });
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mMoviesList.size();
    }

    public void setData(List<MovieModel> dataSet) {
        this.mMoviesList = dataSet;
    }


}
