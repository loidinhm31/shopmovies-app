package com.example.shopmovies;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.shopmovies.login.facebooklogin.FacebookSignInHelper;
import com.example.shopmovies.login.googlelogin.GoogleSignInHelper;

import com.example.shopmovies.login.SharedPrefsManager;
import com.facebook.AccessToken;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.tasks.Task;




public class LoginActivity extends Activity {


    private FacebookSignInHelper facebookSignInHelper;
    private GoogleSignInHelper googleSignInHelper;

    private SharedPrefsManager prefs;

    // variable to save state login to SharedPrefs
    private String loginType;

    // variable to limit multi-type login
    private boolean isFbFirst = true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);



        if (isNetworkAvailable(getBaseContext())) {

            prefs = SharedPrefsManager.getInstance();
            loginType = prefs.getString(this, "LOGIN_TYPE", "not sign in");


            if (loginType.equals("google")) {
                Toast.makeText(this, "You logged with " + loginType, Toast.LENGTH_SHORT).show();

                // Set for first time send to onActivityResult
                isFbFirst = false;

                initializeGoogleControl();
            } else if (loginType.equals("facebook")) {
                Toast.makeText(this, "You logged with " + loginType, Toast.LENGTH_SHORT).show();

                initializeFacebookControl();

                // Check whether user logged with facebook, if true skip this activity by setOnSuccessLogIn
                AccessToken accessToken = AccessToken.getCurrentAccessToken();
                boolean isLoggedIn = accessToken != null && !accessToken.isExpired();
                if (isLoggedIn) {
                    facebookSignInHelper.setOnSuccessLogIn(this);
                }
            } else {
                Toast.makeText(this, "You are " + loginType, Toast.LENGTH_SHORT).show();
                initializeGoogleControl();
                initializeFacebookControl();
            }

        }
    }



    /***********************************************************************************************
     * Checking Network Connection
     **********************************************************************************************/
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        // Get network info for all of the data interfaces (e.g. WiFi, 3G, LTE, etc.)
        NetworkInfo[] info = connectivity.getAllNetworkInfo();


        if (connectivity.getActiveNetworkInfo() == null) {
            Toast.makeText(context, "Network is not available", Toast.LENGTH_LONG).show();
            return false;
        }

        // Make sure that there is at least one interface to test against
        if (info != null) {
        // Iterate through the interfaces
            for (int i = 0; i < info.length; i++) {

        // Check this interface for connected state
                if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                    return true;
                }
            }
        }
        return false;
    }





    /***********************************************************************************************
     * Sign In
     **********************************************************************************************/

    // Facebook Sign in
    private void initializeFacebookControl() {
        facebookSignInHelper = FacebookSignInHelper.newInstance(LoginActivity.this);

        LoginButton loginButton = findViewById(R.id.fb_login_btn);
        loginButton.setReadPermissions("email");
        loginButton.setAuthType("rerequest");
        loginButton.registerCallback(facebookSignInHelper.getCallbackManager(), new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

                // Save state connection with Facebook to SharedPrefs
                prefs = SharedPrefsManager.getInstance();
                prefs.saveLogged_IN(LoginActivity.this, true, "facebook");
                // User login
                facebookSignInHelper.loginUser();
                // Set result for onActivityResult
                setResult(RESULT_OK);
                finish();

            }

            @Override
            public void onCancel() {
                setResult(RESULT_CANCELED);
                finish();
            }

            @Override
            public void onError(FacebookException error) {

            }
        });


    }

    // Google Sign in
    private void initializeGoogleControl() {
        googleSignInHelper = GoogleSignInHelper.newInstance(LoginActivity.this);

        // Set the dimensions of the sign-in button.
        SignInButton signInButton = findViewById(R.id.google_sign_in_btn);
        // Set the dimensions of the sign-in button
        signInButton.setSize(SignInButton.SIZE_STANDARD);

        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Save state connection with Google to SharedPrefs
                prefs = SharedPrefsManager.getInstance();
                prefs.saveLogged_IN(LoginActivity.this, true, "google");

                // Call helper to sign in
                googleSignInHelper.signIn(LoginActivity.this);
            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (isNetworkAvailable(getBaseContext())) {

            if (isFbFirst) {
                // Facebook
                facebookSignInHelper.getCallbackManager().onActivityResult(requestCode, resultCode, data);
                if (resultCode == RESULT_OK) {
                    facebookSignInHelper.setOnSuccessLogIn(this);
                }

            } else {
                // Google
                // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
                if (requestCode == googleSignInHelper.RC_SIGN_IN) {

                    GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
                    // The Task returned from this call is always completed, no need to attach a listener.
                    Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
                    googleSignInHelper.handleSignInResult(task, LoginActivity.this);

                }
            }
        }
    }


}