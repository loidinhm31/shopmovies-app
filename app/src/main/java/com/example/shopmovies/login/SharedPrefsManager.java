package com.example.shopmovies.login;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;


/**
 * Singleton Class for accessing SharedPreferences,
 * should be initialized once in the beginning by any application component using static
 * method initialize(applicationContext)
 */
public class SharedPrefsManager {

    private SharedPreferences prefs;
    private static SharedPrefsManager instance;



    public static SharedPrefsManager getInstance() {
        if (instance == null) {
            synchronized (SharedPrefsManager.class) {
                instance = new SharedPrefsManager();
            }
        }
        return instance;
    }



    /**
     * Clears all data in SharedPreferences
     */
    public void clearPrefs(Context context) {
        prefs = PreferenceManager
                .getDefaultSharedPreferences(context);

        SharedPreferences.Editor editor = prefs.edit();
        editor.clear();
        editor.commit();
    }



    public void saveLogged_IN (Context context, Boolean isLoggedin, String loginType) {
        prefs = PreferenceManager
                .getDefaultSharedPreferences(context);

        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean("IS_LOGIN", isLoggedin);
        editor.putString("LOGIN_TYPE", loginType);
        editor.commit();
    }

    public boolean getLogged_IN (Context context) {
        prefs = PreferenceManager
                .getDefaultSharedPreferences(context);
        return prefs.getBoolean("IS_LOGIN", false);
    }

    public String getString(Context context, String key, String defValue) {
        prefs = PreferenceManager
                .getDefaultSharedPreferences(context);
        return prefs.getString(key, defValue);
    }


    public void setUserInfo (Context context, String id, String avatar, String name, String email, String birthday) {
        prefs = PreferenceManager
                .getDefaultSharedPreferences(context);

        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("USER_ID", id);
        editor.putString("USER_PICTURE", avatar);
        editor.putString("USER_NAME", name);
        editor.putString("USER_EMAIL", email);
        editor.putString("USER_BIRTHDAY", birthday);
        editor.apply();
    }

}
