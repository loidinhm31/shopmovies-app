package com.example.shopmovies.login.facebooklogin;

import android.app.Activity;
import android.content.Intent;

import com.example.shopmovies.ProcessUserInfoActivity;
import com.example.shopmovies.login.SharedPrefsManager;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import java.util.Arrays;
import java.util.Collection;

public class FacebookSignInHelper {


    private static FacebookSignInHelper facebookSignInHelper = null;

    private CallbackManager callbackManager;

    private Activity mActivity;

    // List of permissions to access (some permissions cannot request)
    private static final Collection<String> PERMISSION_LOGIN = (Collection<String>)
            Arrays.asList("public_profile", "email", "birthday");

    private FacebookCallback<LoginResult> loginCallback;

    public static FacebookSignInHelper newInstance(Activity context) {
        if (facebookSignInHelper == null)
            facebookSignInHelper = new FacebookSignInHelper(context);
        return facebookSignInHelper;
    }

    public FacebookSignInHelper(final Activity mActivity) {
        this.mActivity = mActivity;

        // Initialize the SDK before executing any other operations,
        // especially, if you're using Facebook UI elements.
        FacebookSdk.sdkInitialize(this.mActivity);
        callbackManager = CallbackManager.Factory.create();


    }

    /**
     * To login user on facebook without default Facebook button
     */
    public void loginUser() {
        try {
            LoginManager.getInstance().registerCallback(callbackManager, loginCallback);
            LoginManager.getInstance().logInWithReadPermissions(this.mActivity, PERMISSION_LOGIN);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public CallbackManager getCallbackManager() {
        return callbackManager;
    }


    public void setOnSuccessLogIn(Activity activity) {
        Intent intent = new Intent(activity, ProcessUserInfoActivity.class);
        activity.startActivity(intent);
        activity.finish();
    }


    /**
     * To log out user from facebook
     */
    public void signOut() {
        SharedPrefsManager prefs = SharedPrefsManager.getInstance();

        // Change state of login session
        prefs.saveLogged_IN(mActivity, false, null);

        // Clear User Information from SharedPrefs
        prefs.clearPrefs(mActivity);

        // Facebook sign out
        LoginManager.getInstance().logOut();

    }

}

