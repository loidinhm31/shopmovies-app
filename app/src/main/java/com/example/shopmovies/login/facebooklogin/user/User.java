package com.example.shopmovies.login.facebooklogin.user;

import android.net.Uri;

public class User {
    private final String id;
    private final Uri picture;
    private final String name;
    private final String email;
    private final String birthday;
    private final String permissions;

    public User(String id, Uri picture, String name, String email, String birthday, String permissions) {
        this.id = id;
        this.picture = picture;
        this.name = name;
        this.email = email;
        this.birthday = birthday;
        this.permissions = permissions;
    }

    public Uri getPicture() {
        return picture;
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getBirthday() {
        return birthday;
    }

    public String getPermissions() {
        return permissions;
    }
}