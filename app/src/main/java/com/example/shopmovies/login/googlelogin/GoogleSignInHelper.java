package com.example.shopmovies.login.googlelogin;


import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;


import com.example.shopmovies.LoginActivity;

import com.example.shopmovies.ProcessUserInfoActivity;

import com.example.shopmovies.login.SharedPrefsManager;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;



public class GoogleSignInHelper {

    private static final String TAG = GoogleSignInHelper.class.getSimpleName();

    private static GoogleSignInHelper googleSignInHelper = null;
    private Activity mActivity;

    public static final int RC_SIGN_IN = 9001;

    private GoogleSignInClient mGoogleSignInClient;



    public static GoogleSignInHelper newInstance(Activity context) {
        if (googleSignInHelper == null) {
            googleSignInHelper = new GoogleSignInHelper(context);
        }
        return googleSignInHelper;
    }

    public GoogleSignInHelper(Activity mActivity) {
        this.mActivity = mActivity;
        initGoogleSignIn();
    }




    public void initGoogleSignIn() {
        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        // Build facebook_button GoogleSignInClient with the options specified by gso.
        mGoogleSignInClient = GoogleSignIn.getClient(mActivity, gso);
    }





    public void signIn(Activity activity) {
        // Starting the intent prompts the user to select a Google account to sign in with
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();

        // Send result to onActivityResult
        activity.startActivityForResult(signInIntent, RC_SIGN_IN);

    }

    // Handle The Result
    public void handleSignInResult(Task<GoogleSignInAccount> completedTask, Activity activity) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            // Signed in successfully, show authenticated UI.
            updateUI(activity);
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
        }
    }


    public void checkInOnStart(Activity activity) {
        // Check for existing Google Sign In account, if the user is already signed in
        // the GoogleSignInAccount will be non-null.
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(mActivity);
        if (account != null) {
            Toast.makeText(activity, "stuck here", Toast.LENGTH_SHORT).show();
            updateUI(activity);
        } else {
            Toast.makeText(mActivity, "You was not sign in with Google", Toast.LENGTH_SHORT).show();
        }
    }


    // Change the UI when Sign In success
    public void updateUI(Activity activity) {
        Intent intent = new Intent(activity, ProcessUserInfoActivity.class);
        activity.startActivity(intent);
        activity.finish();

    }



    public void signOut() {
        SharedPrefsManager prefs = SharedPrefsManager.getInstance();

        // Change state of login session.
        prefs.saveLogged_IN(mActivity, false, null);

        // Clear User Information from SharedPrefs.
        prefs.clearPrefs(mActivity);

        // Clears which account is connected to the app.
        // To sign in again, the user must choose their account again.
        mGoogleSignInClient.signOut().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                Intent intent = new Intent(mActivity, LoginActivity.class);
                mActivity.startActivity(intent);
                mActivity.finish();
            }
        });

    }






}
